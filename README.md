# GitLab Remote Development WebIDE Docker

## Disclaimer

Until this notice is removed from the README, this repository should be considered to be in [alpha stage](https://about.gitlab.com/handbook/product/gitlab-the-product/#alpha-beta-ga). As such it is not encouraged to run it in production just yet unless you feel capable of debugging yourself any issue that may arise.

## Architecture

![Reference Diagram](./docs/architecture.png)

## Pre-requisites

- Remote machine with root access and docker installed
- Domain address resolving to the above machine

## Usage

### Generating Let's Encrypt Certificates

**Point a domain to your remote machine**

Create `A` record for `example.rd-group.gitlab.vishaltak.com` to `1.2.3.4`

**Installing Certbot**

```shell
sudo apt-get update
sudo apt-get install certbot
```

**Generate certificates**

```shell
export EMAIL="vtak@gitlab.com"
export DOMAIN="example.rd-group.gitlab.vishaltak.com"

certbot -d "${DOMAIN}" \
    -m "${EMAIL}" \
    --config-dir ~/.certbot/config \
    --logs-dir ~/.certbot/logs \
    --work-dir ~/.certbot/work \
    --manual \
    --preferred-challenges dns certonly
```

### Creating workspace

```shell
export CERTS_DIR="/home/ubuntu/.certbot/config/live/${DOMAIN}"
export PROJECTS_DIR="/home/ubuntu"

docker run -d \
    --name my-workspace \
    -p 3443:3443 \
    -v "${CERTS_DIR}/fullchain.pem:/gitlab-rd-web-ide/certs/fullchain.pem" \
    -v "${CERTS_DIR}/privkey.pem:/gitlab-rd-web-ide/certs/privkey.pem" \
    -v "${PROJECTS_DIR}:/projects" \
    gitlab-rd-web-ide:latest --log-level warn --domain "${DOMAIN}" --ignore-version-mismatch
```

> Note: Add the flag `--ignore-version-mismatch` to the above command if your server and client are not on the same version and you'd like to bypass it.

### Fetching token

```shell
docker exec my-workspace cat TOKEN
```

### Connect to WebIDE

```shell
echo "https://gitlab-org.gitlab.io/gitlab-web-ide?remoteHost=${DOMAIN}:3443&hostPath=/projects"
```

Browse the above URL and enter the token from the previous step

### Start Coding

You are all ready to go!

### Stopping workspace

```shell
docker container stop my-workspace
```

### Starting an existing workspace

```shell
docker container start my-workspace
```

> Note: The TOKEN changes every time you start the workspace.

### Deleting workspace

```shell
docker container rm my-workspace
```

## Development

### Build

```shell
export VSCODE_REH_VERSION=1.73.1-1.0.0-dev-20221208024633
export OS=linux
export ARCH=x64

./scripts/prepare.sh \
    --vscode-reh-version "${VSCODE_REH_VERSION}" \
    --os "${OS}" \
    --arch "${ARCH}"

docker build -f Dockerfile -t gitlab-rd-web-ide:latest .
```

### Run

```shell

export DOMAIN="example.rd-group.gitlab.vishaltak.com"
export CERTS_DIR="/home/ubuntu/.certbot/config/live/${DOMAIN}"
export PROJECTS_DIR="/home/ubuntu"

docker container rm -f my-workspace && \
    docker run -d \
        --name my-workspace \
        -p 3443:3443 \
        -v "${CERTS_DIR}/fullchain.pem:/gitlab-rd-web-ide/certs/fullchain.pem" \
        -v "${CERTS_DIR}/privkey.pem:/gitlab-rd-web-ide/certs/privkey.pem" \
        -v "${PROJECTS_DIR}:/projects" \
        gitlab-rd-web-ide:latest --log-level warn --domain "${DOMAIN}" --ignore-version-mismatch && \
    docker logs -f my-workspace
```

### Publish

```shell
# NOTE: Update the value here before proceeding
export IMAGE_VERSION=0.-alpha

docker tag "gitlab-rd-web-ide:latest" "registry.gitlab.com/gitlab-com/create-stage/editor-poc/remote-development/gitlab-rd-web-ide-docker:${IMAGE_VERSION}"

docker login registry.gitlab.com

docker push "registry.gitlab.com/gitlab-com/create-stage/editor-poc/remote-development/gitlab-rd-web-ide-docker:${IMAGE_VERSION}"
```
