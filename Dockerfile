FROM ubuntu:22.04

RUN apt-get update \
  && apt-get install -y git \
  && rm -rf /var/lib/apt/lists/*

COPY ./scripts scripts
COPY ./nginx.conf.template nginx.conf.template
COPY ./vscode-reh vscode-reh

ENTRYPOINT ["./scripts/start.sh"]
