#!/bin/sh

# http://mywiki.wooledge.org/BashFAQ/035
# Usage info
show_help() {
cat << EOF
Usage: ${0##*/} [-h] [--gitlab-token TOKEN]...
Fetch details about package dependencies (VS Code Remote Extension Host)

    -h | --help          display this help and exit
    --gitlab-token       gitLab token
    --vscode-reh-version VS Code Remote Extension Host Version
    --os                 OS for which VS Code REH download link is to be fetched
    --arch               architecture for which VS Code REH download link is to be fetched
EOF
}

die() {
    printf '%s\n' "$1" >&2
    exit 1
}

# Initialize all the option variables.
# This ensures we are not contaminated by variables from the environment.
VSCODE_REH_VERSION="1.73.1-1.0.0-dev-20221208024633"
OS=$(uname -s | tr "[:upper:]" "[:lower:]")
ARCH=$(uname -m)
if [ $ARCH = "x86_64" ]; then
    ARCH="x64"
fi

while :; do
    case $1 in
        -h|-\?|--help)
            show_help    # Display a usage synopsis.
            exit
            ;;
        --vscode-reh-version)
            if [ "$2" ]; then
                VSCODE_REH_VERSION=$2
                shift
            else
                die 'ERROR: "--vscode-reh-version" requires a non-empty option argument.'
            fi
            ;;
        --os)
            if [ "$2" ]; then
                case $2 in
                    linux|win32|darwin)
                        OS=$2
                        ;;
                    *)
                        die 'ERROR: "--os" can be only be linux|win32|darwin.'
                        ;;
                esac
                shift
            else
                die 'ERROR: "--os" requires a non-empty option argument.'
            fi
            ;;
        --arch)
            if [ "$2" ]; then
                case $2 in
                    x64|arm64)
                        ARCH=$2
                        ;;
                    *)
                        die 'ERROR: "--arch" can be only be x64|arm64.'
                        ;;
                esac
            else
                die 'ERROR: "--arch" requires a non-empty option argument.'
            fi
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac

    shift
done

# Rest of the program here.

. ./scripts/env.sh

# fetch download link for vscode reh
VSCODE_REH_TAR_FILE="vscode-reh-${OS}-${ARCH}-${VSCODE_REH_VERSION}.tar.gz"

PACKAGE_ID=$(curl "https://gitlab.com/api/v4/projects/gitlab-org%2Fgitlab-web-ide-vscode-fork/packages" | jq --arg VSCODE_REH_VERSION "${VSCODE_REH_VERSION}" '.[] | select(.version==$VSCODE_REH_VERSION) | .id')

ASSET_ID=$(curl "https://gitlab.com/api/v4/projects/gitlab-org%2Fgitlab-web-ide-vscode-fork/packages/${PACKAGE_ID}/package_files" | jq --arg VSCODE_REH_TAR_FILE "${VSCODE_REH_TAR_FILE}" '.[] | select(.file_name==$VSCODE_REH_TAR_FILE) | .id')

DOWNLOAD_LINK="https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/package_files/${ASSET_ID}/download"

echo "Downloading file ${DOWNLOAD_LINK}"

curl "${DOWNLOAD_LINK}" --output "${VSCODE_REH_TAR_FILE}"
if [ -d "${VSCODE_REH_DIR}" ]; then rm -rf ${VSCODE_REH_DIR}; fi
mkdir "${VSCODE_REH_DIR}"
tar -xf "${VSCODE_REH_TAR_FILE}" -C "./${VSCODE_REH_DIR}"
rm "${VSCODE_REH_TAR_FILE}"
